

#include <Arduino.h>
#include "variant.h"
#include <due_can.h>
#include <Wire.h>
#include <Adafruit_INA219.h>
#include "json.h"

#define CanChambreIO Can1
#define CanPrincipalIO Can0






boolean TestChambre();
boolean Test_CANPrincipal();
void SendDataCan1(char *InputBuffer);
void GetSerie();
void GetSoft(void);
void TestHardware();
boolean TestESPA();
void SendDataCan0(char* InputBuffer);
void CanPrincipalReception();
void getCurrent ();
void init_current_senssor();
float get_voltage();
void SendDataCan1_aved_id(long id, char *InputBuffer);
long CanPrincipalReception_id(void);
bool get_mac(void);
void CanChambreReception(void);
void check_230v();
