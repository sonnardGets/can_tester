#include <Arduino.h>



#include "json.h"
#include "tests.h"
#include "bus_poire.h"
#include "delay_ns.h"
#include "can_library_ns.h"
#include "defines.h"
// ceci est test


String inputString = "";     // a String to hold incoming data
bool stringComplete = false; // whether the string is complete
int J;

char InputBufer[500];

#define DataTimeMax 100
#define DataTimeMin 90

int i = 0;

void setup()
{
  // set la led d'alimentation en sortie
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);
  pinMode(5, INPUT_PULLUP);
  pinMode(41, OUTPUT);
  pinMode(RESET, OUTPUT);
  pinMode(RESET_STLINK, OUTPUT);
  digitalWrite(41, HIGH);
  digitalWrite(RESET, HIGH);
  digitalWrite(RESET_STLINK, HIGH);

  Serial.begin(115200);
  Serial3.begin(115200);
  Serial2.begin(1200, SERIAL_7E2);
  // Initialize CAN0 and CAN1, Set the proper baud rates here
  CanPrincipalIO.begin(CAN_BPS_125K);
  CanChambreIO.begin(CAN_BPS_125K);

  CanPrincipalIO.watchFor();
  CanChambreIO.watchFor();
  Serial3.println("Ready");
}

void loop()
{

  check_230v();
  // le courant est en mA
  getCurrent();
  CanPrincipalReception();
  CanChambreReception();
  // heartbeat et signal que l'arduino est prêt à recevoir un data
  Serial.println("?");
  delay(100);
}

/*

    Regarde si il y a des données dans le buffer de l'uart

*/

void serialEvent()
{
  while (Serial.available())
  {

    // get the new byte:
    char inChar = (char)Serial.read();

    // test le len d'un message et l'ignore si il comporte plus de 500 caractere

    if (J > 500)
    {
      Serial.println("{\"typemsg\":\"communication\",\"Error\":{\"type\":\"len_error\"}}");
      return;
      J = 0;
      memset(InputBufer, 0, 500);
    }
    if (inChar == '\n')
    {

      stringComplete = true;
      J = 0;
      json_traitement(InputBufer);
      memset(InputBufer, 0, 500);
    }
    else
    {

      InputBufer[J] = inChar;
      J++;
    }
  }
}