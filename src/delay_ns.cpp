/*
 * Filename: c:\Users\Sonnardni\Documents\Arduino\lecture_bus_gets_poires\delay_ns.c
 * Path: c:\Users\Sonnardni\Documents\Arduino\lecture_bus_gets_poires
 * Created Date: Tuesday, February 11th 2020, 5:15:46 pm
 * Author: Nicolas.Sonnard
 * 
 * Copyright (c) 2020 Your Company
 */

#include "delay_ns.h"

void Delay1us(void)
{
  int j;
  for (j = 0; j < 13; j++)
  {
    //delayMicroseconds(1);
    //__asm__ __volatile__("nop");
    asm volatile("nop" ::);
  }
}

void Delayus(int us)
{
  int j;
  int m;
  for (m = 0; m < us; m++)
  {
    for (j = 0; j < 13; j++)
    {
      //delayMicroseconds(1);
      //__asm__ __volatile__("nop");
      asm volatile("nop" ::);
    }
  }
}
