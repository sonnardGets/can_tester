
/*

    test dus bus can de chambre sur Can0


*/

#include "can_library_ns.h"

//#define DEBUG

/**

   programme de tests des Ã©lectroniques CAN

   le soft recevra des ordres du pc et effectura diffÃ©rents tests, certaines commande permetront d'intÃ©ragir avec l'Ã©lectronique

   l'arduino testera les deux port can des Ã©lectronique.

   un mode bridge permetra au pc d'envoyer n'importe quelle commande CAN soit au port 0 soit au Port un de l'arduino
*/

// utilisation du port Natif usb afin de profiter des fonctions suplÃ©mentaires

//Leave defined if you use native port, comment if using programming port
//#define Serial SerialUSB

bool TestChambre()
{
  long lastTime;

  // :X2900000N000000;
  CAN_FRAME RoomFrame;
  RoomFrame.id = 0x2900000;
  RoomFrame.extended = true;
  RoomFrame.priority = 4; //0-15 lower is higher priority

  CanChambreIO.sendFrame(RoomFrame);

  lastTime = millis();
  // on test pendant 3 sec le can principal
  while ((millis() - lastTime) < 3000)
  {
    CanChambreIO.sendFrame(RoomFrame);
    delay(10);

    if (CanChambreIO.available() > 0)
    {

      CanChambreIO.read(RoomFrame);
      if (RoomFrame.id > 1)
      {
        // simple test du CAN si ce data est vu la ligne est OK.
        return true;
      }
    }

    CanChambreIO.sendFrame(RoomFrame);
  }
  return false;
}

/*

  test dus bus can principal sur CAN1



*/

bool Test_CANPrincipal()
{
  long lastTime;
  float TimeOutCanPrincipal;
  CAN_FRAME Principal;
  Principal.id = 0x1198141;
  Principal.length = 2;
  Principal.data.bytes[0] = 0xFF;
  Principal.data.bytes[1] = 0xFF;
  Principal.data.bytes[2] = 0xFF;
  Principal.data.bytes[3] = 0xFF;

  Principal.extended = true;
  Principal.priority = 4; //0-15 lower is higher priority

  CanPrincipalIO.sendFrame(Principal);

  lastTime = millis();
  // on test pendant 3 sec le can principal
  while ((millis() - lastTime) < 1000)
  {

    if (CanPrincipalIO.available() > 0)
    {

      TimeOutCanPrincipal = millis();
      CanPrincipalIO.read(Principal);
      if (Principal.id > 1)
      {
        // simple test du CAN si ce data est vu la ligne est OK.
        return true;
      }
    }
    CanPrincipalIO.sendFrame(Principal);
    delay(500);
  }
  // TEST DU CAN AVEC UN AUTRE PACKET --> POUR LES CONFIGURATEUR
  Principal.id = 0x1400000;
  Principal.length = 2;
  Principal.data.bytes[0] = 0xFF;
  Principal.data.bytes[1] = 0xFF;
  Principal.data.bytes[2] = 0xFF;
  Principal.data.bytes[3] = 0xFF;

  Principal.extended = true;
  Principal.priority = 4; //0-15 lower is higher priority

  CanPrincipalIO.sendFrame(Principal);

  lastTime = millis();
  // on test pendant 3 sec le can principal
  while ((millis() - lastTime) < 1000)
  {

    if (CanPrincipalIO.available() > 0)
    {

      TimeOutCanPrincipal = millis();
      CanPrincipalIO.read(Principal);
      if (Principal.id > 1)
      {
        // simple test du CAN si ce data est vu la ligne est OK.
        return true;
      }
    }
    CanPrincipalIO.sendFrame(Principal);
    delay(500);
  }

  return false;
}

/*


  Envoie la version de soft



*/

void GetSoft(void)
{
  long Version;
  long lastTime;
  CAN_FRAME Principal;
  Principal.id = 0x1198141;
  Principal.length = 2;
  Principal.data.bytes[0] = 0xFF;
  Principal.data.bytes[1] = 0xFF;
  Principal.data.bytes[2] = 0xFF;
  Principal.data.bytes[3] = 0xFF;

  Principal.extended = true;
  Principal.priority = 4; //0-15 lower is higher priority

  CanPrincipalIO.sendFrame(Principal);
  lastTime = millis();
  // on test pendant 3 sec le can principal
  while ((millis() - lastTime) < 3000)
  {
    if (CanPrincipalIO.available() > 0)
    {

      CanPrincipalIO.read(Principal);

      Version = Principal.data.bytes[5];
      Version = (Version << 8) | Principal.data.bytes[6];
      if (Version < 9999)

      {
        Serial.print("{\"typemsg\":\"");
        Serial.print("GUI");
        Serial.print("\",\"soft\":\"");
        Serial.print(Principal.data.bytes[4] / 10);
        Serial.print(".");
        Serial.print(Principal.data.bytes[7] / 10);
        Serial.print(",V");
        Serial.print(Version);
        Serial.println("\"}");
      }
      else
      {
        Serial.print("{\"typemsg\":\"");
        Serial.print("GUI");
        Serial.print("\",\"soft\":\"");
        Serial.print("erreur de la demande");
        Serial.println("\"}");
      }
      return;
    }
  }
  Serial.print("{\"typemsg\":\"");
  Serial.print("GUI");
  Serial.print("\",\"soft\":\"");
  Serial.print("timout de la demande");
  Serial.println("\"}");
}

/*

  demande le numero de serie Ã  l'Ã©lectronique et le renvoie sur l'uart
  Envoie la version de soft avec



*/

void GetSerie(void)
{
  long timeout;
  String no_serie = "";
  CAN_FRAME Principal;
  Principal.id = 0x1190000;
  Principal.length = 2;
  Principal.data.bytes[0] = 0xFF;
  Principal.data.bytes[1] = 0xFF;
  Principal.data.bytes[2] = 0xFF;
  Principal.data.bytes[3] = 0xFF;

  Principal.extended = true;
  Principal.priority = 4; //0-15 lower is higher priority

  CanPrincipalIO.sendFrame(Principal);
  timeout = millis();
  // on test pendant 3 sec le can principal
  while (((Principal.id & 0xFFF0000) != 0x11a0000) && ((millis() - timeout) < 20000))
  {

    if (CanPrincipalIO.available() > 0)
    {

      CanPrincipalIO.read(Principal);

      if (((Principal.id & 0xFFF0000) == 0x11a0000))
      {

        // envoie au banc de test sous forme de JSON le numero de série d'un produit
        /*

        Informations sur l’électronique paquet 1 

        Numéro de série (4 octets), version du soft (4 octets) 

        */

        Serial.print("{\"typemsg\":\"GUI\",\"no_serie\":\"");
        for (int count = 0; count < 8; count++)
        {
          if (Principal.data.bytes[count] < 10)
            Serial.print("0");
          Serial.print(Principal.data.bytes[count], HEX);
        }
        Serial.println("\"}");
        return;
      }

      Serial.println("{\"typemsg\":\"GUI\",\"no_serie\":\"?\"");

    }
  }
}

/*

demande la mac adresse au produit

*/

bool get_mac(void)
{
  long timeout;
  String no_serie = "";
  CAN_FRAME Principal;
  Principal.id = 0x3880000;
  Principal.length = 2;
  Principal.data.bytes[0] = 0xFF;
  Principal.data.bytes[1] = 0xFF;
  Principal.data.bytes[2] = 0xFF;
  Principal.data.bytes[3] = 0xFF;

  Principal.extended = true;
  Principal.priority = 4; //0-15 lower is higher priority

  CanPrincipalIO.sendFrame(Principal);
  timeout = millis();
  // on test pendant 3 sec le can principal
  while (((Principal.id & 0xFFF0000) != 0x3870000) && ((millis() - timeout) < 20000))
  {
    if (CanPrincipalIO.available() > 0)
    {

      CanPrincipalIO.read(Principal);

      if (((Principal.id & 0xFFF0000) == 0x3870000))
      {

        // envoie au banc de test sous forme de JSON le numero de série d'un produit
        Serial.print("{\"typemsg\":\"GUI\",\"mac_adresse\":\"");
        for (int count = 0; count < Principal.length; count++)
        {
          if (Principal.data.bytes[count] < 10)
            Serial.print("0");
          Serial.print(Principal.data.bytes[count], HEX);
        }
        Serial.println("\"}");
        return true;
      }
    }
  }
  return false;
}

/*

  test dus bus can principal sur CAN1



*/

void TestHardware(void)
{
  //:XB48141NFFFF0102;
  //  :XB48141NFFFFFFFF00010002;

  long lastTime;
  CAN_FRAME Principal;

  Principal.length = 4;
  Principal.extended = true;
  Principal.priority = 4; //0-15 lower is higher priority
  Principal.data.bytes[0] = 0xFF;
  Principal.data.bytes[1] = 0xFF;
  Principal.data.bytes[2] = 0x01;
  Principal.data.bytes[3] = 0x02;
  // incrÃ©ment de l'id ( on ou off) +0x10000 passer B4 Ã  B5
  for (long i = 0xB48141; i <= 0xB58141; i += 0x10000)
  {

    Principal.id = i;
    for (int j = 0; j < 3; j++)
    {
      // selection des indicateurs (led1, 2 ou buzzer)
      Principal.data.bytes[2] = j;

      // couleur des leds --> on triche avec le buzzer mais il le prend bien
      for (int k = 0; k < 10; k++)
      {
        Principal.data.bytes[3] = k;
        CanPrincipalIO.sendFrame(Principal);
        // on enlÃ¨ve le dÃ©lais si on Ã©tein et sur le buzzer afin de pas rendre les gens fous
        if (i == 0xb48141 && k != 0x02)
          delay(700);
        else
          delay(2);
      }
    }
  }
}

/*

  Send data sur le can avec l'id du configurateur



*/

void SendDataCan1(char *InputBuffer)
{
  long id = 0xB48141;
  int LenCode = strlen(InputBuffer);
  // on divise par deux le len car 2 octets
  LenCode = LenCode / 2;
  long DataTable[20] = {0};

  CanPrincipalIO.setBigEndian(false);
  CAN_FRAME Principal;
  // on force l'id du configurateur pour l'envoie des datas
  Principal.id = id;
  Principal.length = LenCode;

  // on test si le premier caractÃ¨re est ":" sinon tous est ignorÃ© et on reset

#ifdef DEBUG
  Serial.print("len du data --> ");
  Serial.println(LenCode);
  Serial.print("reception du data CAN  --> ");
#endif

  // on extrait le code et le data du CAN
  sscanf(InputBuffer, "%2x %2x %2x %2x %2x %2x %2x %2x", &(DataTable[0]), &(DataTable[1]), &(DataTable[2]), &(DataTable[3]), &(DataTable[4]), &(DataTable[5]), &(DataTable[6]), &(DataTable[7]));

  // envoie des data Can
  for (int count = 0; count <= LenCode; count++)
  {
#ifdef DEBUG
    Serial.print(InputBuffer);
#endif
    Principal.data.bytes[count] = DataTable[count];
  }
  CanPrincipalIO.sendFrame(Principal);
}



//

/**

  DonnÃ©es sur le BUS CAN principal
  traitement selon plusieurs CAS

*/

void CanPrincipalReception(void)
{
  CAN_FRAME incoming;
  incoming.extended = true;
  long TimeOutCanPrincipal;

  if (CanPrincipalIO.available() > 0)
  {
    TimeOutCanPrincipal = millis();
    CanPrincipalIO.read(incoming);

    //if ((incoming.id & 0xFFF0000) == 0x1360000 || (incoming.id & 0xFFF0000) == 0x3850000 || (incoming.id & 0xFFF0000) == 0x3870000)
    if (incoming.id)
    {
      Serial.print("{\"typemsg\":\"");
      Serial.print("GUI");
      Serial.print("\",\"can_principal\":{\"code\":\"");
      Serial.print(incoming.id, HEX);
      Serial.print("\",\"datas\":\"");
      for (int count = 0; count < incoming.length; count++)
      {
        if (incoming.data.bytes[count] < 10)
          Serial.print("0");
        Serial.print(incoming.data.bytes[count], HEX);
      }
      Serial.println("\"}}");
    }
  }
}


/**

  DonnÃ©es sur le BUS CAN principal
  traitement selon plusieurs CAS

*/

void CanChambreReception(void)
{
  CAN_FRAME incoming;
  incoming.extended = true;
  long TimeOutCanPrincipal;

  if (CanChambreIO.available() > 0)
  {
    TimeOutCanPrincipal = millis();
    CanChambreIO.read(incoming);

    //if ((incoming.id & 0xFFF0000) == 0x1360000 || (incoming.id & 0xFFF0000) == 0x3850000 || (incoming.id & 0xFFF0000) == 0x3870000)
    if (incoming.id)
    {
      Serial.print("{\"typemsg\":\"");
      Serial.print("GUI");
      Serial.print("\",\"can_chambre\":{\"code\":\"");
      Serial.print(incoming.id, HEX);
      Serial.print("\",\"datas\":\"");
      for (int count = 0; count < incoming.length; count++)
      {
        if (incoming.data.bytes[count] < 10)
          Serial.print("0");
        Serial.print(incoming.data.bytes[count], HEX);
      }
      Serial.println("\"}}");
    }
  }
}


/*
    Si le deuxième arduino envoie des données on les transmets au pc (bypass de l'uart)


*/

void serialEvent3()
{

  while (Serial3.available())

    Serial.print(char(Serial3.read()));
}

/*

     test de L ESPA
     envoie uen live check et attend une réponse
     Renvoie un time out si pas de réponse en 2 secondes
     renvoie une erreur si le live check n'est pas EOT

*/

boolean TestESPA(void)
{

  Serial2.print("2\u0005");
  delay(3000); // delay in between reads for stability
  int TimeOutESPA = 0;
  boolean testESPA = false;
  while (TimeOutESPA < 2000 && testESPA == false)
  {
    while (Serial2.available())
    {

      char inChar = Serial2.read();
      // si un EOT est detecté alors on valide le test
      if (inChar == 0x04)
      {
        json_send_terminal("espa OK");
        testESPA = true;
        return true;
      }
      else
      {
        json_send_terminal("espa KO");
        return false;
      }
    }
    delay(1);
    TimeOutESPA++;
  }
  if (TimeOutESPA >= 2000)
  {
    json_send_terminal("espa Timeout");
    return false;
  }
}

void getCurrent(void)
{
  static double old_current = 0;
  Adafruit_INA219 ina219;
  ina219.begin();

  int I = 0;
  double current_total = 0;
  int sample = 200;

  for (I = 0; I <= sample; I++)

  {
    current_total = current_total + ina219.getCurrent_mA();
    delay(2);
  }

  // envoie au bans de test que si il y a un delta de 10 mA calcule fait avec une valeur absolue
  if (fabs((current_total / sample) - old_current) >= 10)
  {
    json_send_current(current_total / sample);
    old_current = current_total / sample;
  }
}

float get_voltage(void)
{
  Adafruit_INA219 ina219;
  ina219.begin();

  int I = 0;
  float u_total = 0;
  int sample = 10;

  for (I = 0; I <= sample; I++)

  {
    u_total = u_total + ina219.getBusVoltage_V();
    delay(2);
  }

  return (u_total / sample);
}
/*

controle si la prise du banc de test est connectée.

*/


void check_230v()
{
    if (get_voltage() <5)
  {
    json_send_infos_box("Prise secteur erreur");
   
    while (get_voltage() <5)
    {
      delay(100);
    }
     
    json_send_infos_box(" ");
  }
}

/*

  Send data sur le can avec un id à choix



*/

void SendDataCan1_aved_id(long id, char *InputBuffer)
{

  int LenCode = strlen(InputBuffer);
  // on divise par deux le len car 2 octets
  LenCode = LenCode / 2;
  long DataTable[20] = {0};

  CanPrincipalIO.setBigEndian(false);
  CAN_FRAME Principal;

  Principal.length = LenCode;
  Principal.extended = true;
  Principal.priority = 4; //0-15 lower is higher priority
  Principal.id = id;

  // on test si le premier caractÃ¨re est ":" sinon tous est ignorÃ© et on reset

#ifdef DEBUG
  Serial.print("len du data --> ");
  Serial.println(LenCode);
  Serial.print("reception du data CAN  --> ");
#endif

  // on extrait le code et le data du CAN
  sscanf(InputBuffer, "%2x %2x %2x %2x %2x %2x %2x %2x", &(DataTable[0]), &(DataTable[1]), &(DataTable[2]), &(DataTable[3]), &(DataTable[4]), &(DataTable[5]), &(DataTable[6]), &(DataTable[7]));

  // envoie des data Can
  for (int count = 0; count <= LenCode; count++)
  {
#ifdef DEBUG
    Serial.print(InputBuffer);
#endif
    Principal.data.bytes[count] = DataTable[count];
  }
  CanPrincipalIO.sendFrame(Principal);
}

/**

  DonnÃ©es sur le BUS CAN principal
  traitement selon plusieurs CAS

*/

long CanPrincipalReception_id(void)
{
  CAN_FRAME incoming;
  incoming.extended = true;
  long TimeOutCanPrincipal;

  if (CanPrincipalIO.available() > 0)
  {

    TimeOutCanPrincipal = millis();
    CanPrincipalIO.read(incoming);

    //if ((incoming.id & 0xFFF0000) == 0x1360000 || (incoming.id & 0xFFF0000) == 0x3850000 || (incoming.id & 0xFFF0000) == 0x3870000)
    if (incoming.id)
    {
      return incoming.id;
      //Serial.print(incoming.id, HEX);
    }
  }
}
