/*
 * Filename: c:\Users\Sonnardni\Documents\Arduino\lecture_bus_gets_poires\tests.c
 * Path: c:\Users\Sonnardni\Documents\Arduino\lecture_bus_gets_poires
 * Created Date: Tuesday, February 11th 2020, 5:06:40 pm
 * Author: Nicolas.Sonnard
 * 
 * Copyright (c) 2020 Your Company
 */

#include "tests.h"

/*
 synchronise le bus et test les sorties des relais

 param entran nombres de relais à tester

 param sortant aucuns

*/

/*

    traite les json et lance les différents tests
    reception -   json du port serial
    json à envoyer pour les tests
    {"typemsg": "communication","tests": {"Boutons": true,"P4": true,"OPTO2-7": true,"Bouton_prise": true}} 

{'productNumber': '110039', 'tests': {'Boutons': [], 'Can_principal': [], 'Can_chambre': []}}

    attention il faut mettre un retour à la ligne pour que la commande soit comprise par le banc de tests

*/

void json_traitement(char *InputBufer)
{

  // desactive tous les optos

  digitalWrite(T2cmd, LOW);
  digitalWrite(T3cmd, LOW);
  digitalWrite(ACCcmd, LOW);
  digitalWrite(Appele_jack, LOW);
  digitalWrite(A1cmd, LOW);

  JsonParser<500> parser;
  JsonHashTable hashTable = parser.parseHashTable(InputBufer);
  Serial.println(InputBufer);


  if (hashTable.getString("infos_product"))
  {
    GetSerie();
    get_mac();
  }
  /*
    envoie de l'adresse mac 

  */
  if (hashTable.getString("mac_adresse"))
  {
    // envoie sur le bus can les datas --> on part du principe que l'identifiant sera celui du configurateur
    prog_adresse_mac(hashTable.getString("mac_adresse"));
  }

  /*
    envoie du numero de série

  */
  if (hashTable.getString("no_serie"))
  {
    // envoie sur le bus can les datas --> on part du principe que l'identifiant sera celui du configurateur
    SendDataCan1_aved_id(0x3840000, hashTable.getString("no_serie"));
  }

  // test et renvoie une erreur si le premier parsage à pas fonctionné
  if (!hashTable.success())
  {
    Serial.println("{\"typemsg\":\"communication\",\"Error\":{\"type\":\"parsing_error\"}}");
    return;
  }
  // clear du terminal
  clear_main_terminal();
  // demande de la version de soft
  GetSoft();
  // test si il faut envoyer un data sur le can
  if (hashTable.getString("can_principal"))
  {
    // envoie sur le bus can les datas --> on part du principe que l'identifiant sera celui du configurateur
    SendDataCan1(hashTable.getString("can_principal"));
  }

  // reset le STLINK
  if (hashTable.containsKey("reset_stlink"))
  {
    reset_stlink();
    return;
  }

  // reset le 24 v de l'électronique connectee
  if (hashTable.containsKey("reset"))
  {
    reset_24V();
    return;
  }

  // test si le json comporte des tests
  if (hashTable.getString("tests"))
  {
    char *sensor = hashTable.getString("tests");
    JsonHashTable tests_google = parser.parseHashTable(sensor);
    if (!hashTable.success())
    {
      json_send_error("communication", "parsing_test_error");
      json_send_terminal("Error avec le JSON");
      return;
    }

    // call des test si ils sont présent dans le JSON on les fait. Le json affichera toujours le test TRUE et si le test existe pas il sera jamais affiché c'est pour ça
    // qu'on regarde la presence du terme sans prendre compte de sa valeur.
    if (tests_google.containsKey("OPTO1-10"))
    {

      json_send_terminal("tests des opto 1-10");
      if (tests_p2p4p5(0, 10))
      {
        json_send_terminal("opto 1- 10 ok");
      }
      else
      {
        test_result("test des sorties relais KO", false);
        return;
      }
    }
    if (tests_google.containsKey("Boutons"))
    {
      if (test_boutton())
      {
        json_send_terminal("Bouton/s OK");
      }

      else
      {
        test_result("test bouton/s KO", false);
        return;
      }
    }
    if (tests_google.containsKey("OPTO2-7"))
    {
      json_send_terminal("tests des opto 2-6");

      if (tests_p2p4p5(2, 6))
      {
        json_send_terminal("test des opto 2-6 ok");
      }
      else
      {
        test_result("test des sorties relais 2-6  KO", false);
        return;
      }
    }
    if (tests_google.containsKey("Bouton_prise"))
    {
      json_send_terminal("tests du bouton de la prise");
      if (test_bouton())
      {
        json_send_terminal("OK");
      }
      else
      {

        test_result("test du bouton KO", false);
        return;
      }
    }
    if (tests_google.containsKey("espa"))
    {
      json_send_terminal("tests de l'espa");
      if (TestESPA())
      {
        json_send_terminal("OK");
      }
      else
      {

        test_result("test ESPA KO", false);
        return;
      }
    }

    if (tests_google.containsKey("P4"))
    {
      json_send_terminal("tests du bornier P4 ");

      if (appels_electronique())
      {
        json_send_terminal("opto 1- 10 ok");
      }
      else
      {
        test_result("test appels electronique", false);
        return;
      }
    }
    // ici on parse le json si il est contenu dans le JSON codes_poire
    if (tests_google.containsKey("buttons"))
    {

      char *tests_json = hashTable.getString("codes_poire");
      if (test_poire(tests_json))
      {
      }
      else
      {
        return;
      }
    }

    /****************************************************
     * 
     * test du can principal
     * 
     * **************************************************/

    if (tests_google.containsKey("Can_principal"))
    {
      if (Test_CANPrincipal())
      {
        test_result("Can_principal ok", true);
      }
      else
      {
        test_result("Can_principal ko", false);
        return;
      }
    }

    /****************************************************
     * 
     * test du can de chambre
     * 
     * **************************************************/

    if (tests_google.containsKey("Can_chambre"))
    {
      if (TestChambre())
      {
        test_result("Can_chambre ok", true);
      }
      else
      {
        test_result("Can_chambre ko", false);
        return;
      }
    }
    /******************************************************
     * 
     *  test du convertisseur gets/gec
     * 
     * ****************************************************/
    if (tests_google.containsKey("gets_gec"))
    {
      if (test_gec_gets())
      {
        test_result("gets/gec ok", true);
      }
      else
      {
        test_result("gets/gec ko", false);
        return;
      }
    }

    /****************************************************
     * 
     * demande du numéro de série 
     * on part du principe que si il y a un can principal dans les test
     * il y aura un numéro de serie
     * 
     * * CES FONCTIONS DOIVENT TOUJOURS ÊTRE à LA FIN
     * 
     * **************************************************/

    if (tests_google.containsKey("Can_principal"))
    {
      // demande du numéro de série au GUI
      GetSerie();
    }

    /****************************************************
     * 
     * test de la partie IP des modules
     * 
     * **************************************************/

    /****************************************************
     * 
     * demande de la mac adresse au gui
     * 
     * CES FONCTIONS DOIVENT TOUJOURS ÊTRE à LA FIN
     * 
     * **************************************************/

    if (tests_google.containsKey("Network"))
    {

      if (get_mac())
      {
        // demande de la mac adresse au GUI
        Serial.println("{\"typemsg\":\"GUI\",\"mac_adresse\":\"?\"}");
      }
      else
      {
        return;
      }
    }

    else
    {
      test_result("tests OK", true);

      json_send_terminal("Les tests sont tous OK BRAVO !");
      Serial.print("{\"typemsg\":\"tests\",\"result\":{\"global_tests\":\"");
      Serial.print("true");
      Serial.println("\"}}");
    }
  }
  /*
  else
  {
    json_send_terminal("Erreur JSON");
    json_send_error("communication", "wrongJSON");
    return;
  }
  */
}

boolean tests_p2p4p5(int relay_1, int nb_relay)
{

  /*
*
* Table des datas a envoyer
*
*/

  uint8_t Datas_Bus[] = {0, 1, 0, 2, 0, 3, 0, 4, 0, 5, 0, 6, 0, 7, 0, 8, 0, 9, 0, 10};
  uint8_t pin_mesure_p4[] = {4, 3, 5};
  uint8_t commande_p4[] = {11, 12, 14};

  //uint8_t datas_bus_10_optos[] = {0, 1, 0, 2, 0, 3, 0, 4, 0, 5};

  int Compteur_Datas,
      compteur_p4;

  // test des opto
  for (Compteur_Datas = relay_1; Compteur_Datas < (nb_relay * 2); Compteur_Datas++)
  {
    lecture_Bus();
    //lecture_Bus();
    //delayMicroseconds(1200);
    delayMicroseconds(5200);
    SendDatas(0, Datas_Bus[Compteur_Datas]);
    delayMicroseconds(1200);
    SendDatas(0, Datas_Bus[Compteur_Datas]);
    delayMicroseconds(1000);
    //json_send_terminal("test du data  ");
    //json_send_value_terminal(Datas_Bus[Compteur_Datas]);
    //json_send_value_terminal(Compteur_Datas);

    if (relay_test(Datas_Bus[Compteur_Datas]))
    {
    }
    else
    {

      json_send_terminal("Erreur sortie");
      json_send_value_terminal(Datas_Bus[Compteur_Datas]);

      json_send_msgbox_gui("Error Sortie-->  ", 4);
      return false;
    }
  }
  // test du bornier p4

  for (compteur_p4 = 0; compteur_p4 < 3; compteur_p4++)
  {

    lecture_Bus();
    delayMicroseconds(5200);
    SendDatas(0, commande_p4[compteur_p4]);
    delayMicroseconds(1200);
    SendDatas(0, commande_p4[compteur_p4]);
    if (mesure_pinx(pin_mesure_p4[compteur_p4]))
    {
      //json_send_terminal("test de la pin ");
      //json_send_value_terminal(commande_p4[compteur_p4]);
    }
    else
    {
      json_send_terminal("Erreur sur la pin ");
      json_send_value_terminal(commande_p4[compteur_p4]);

      json_send_msgbox_gui("Error-P4", 4);
      return false;
    }
  }
  // test du courcircuit sur le jack --> doit tirer à 0 A1
  digitalWrite(Appele_jack, HIGH);
  if (digitalRead(A1cmes) == LOW)
  {
    //json_send_terminal("ACC-OK");
  }
  else
  {
    {
      json_send_msgbox_gui("Error-ACC", 4);
      return false;
    }
  }
  return true;
}

/*
*
* messure un pin pour attendre qu'elle passe ä LOW
* entrant    numero de pin
*  return    true ou false
*
*/
boolean mesure_pinx(int pin)
{
  int compteur_timout;

  while (compteur_timout < 200 && digitalRead(pin) == HIGH)
  {
    compteur_timout++;
    delay(1);
  }
  if (compteur_timout >= 200)
  {
    return false;
  }
  else
  {
    return true;
  }

  {
    /* code */
  }
}

/*
*
* simule et test les sortie A1 ACC T2 T3
* il faut mettre du 24 V, le couper et voir si la lumière s'allume sur l'électronique
* 
*
*/

boolean appels_electronique()
{
  // on comute l'opto pour envoyer du 24 v à l'electronqiue
  digitalWrite(T2cmd, HIGH);
  delay(500);
  //lecture du data sur le bus afin de voir si la prise à compris.
  if (lecture_Bus() == 2)
  {
    //json_send_terminal("T2-OK");
  }

  else
  {
    json_send_msgbox_gui("Error-T2  ", 4);
    return false;
  }
  digitalWrite(T2cmd, LOW);

  digitalWrite(T3cmd, HIGH);
  delay(500);

  if (lecture_Bus() == 4)
  {
    //json_send_terminal("T3-OK");
  }

  else
  {
    json_send_msgbox_gui("Error-T3  ", 4);
    return false;
  }
  digitalWrite(T3cmd, LOW);

  /*
digitalWrite(ACCcmd,HIGH);

delay (500);
Serial.println("test ACC");
lecture_Bus();



if (digitalRead(ACCcmes))
{

}

else
{
  Serial.println("okACC");
}


digitalWrite(ACCcmd,LOW);
*/

  digitalWrite(A1cmd, HIGH);
  delay(500);

  if (lecture_Bus() == 10)
  {
    //json_send_terminal("A1 \t\t\t OK");
  }

  else
  {
    json_send_msgbox_gui("Error A1  ", 4);
    return false;
  }

  digitalWrite(A1cmd, LOW);

  return true;
}

/*
*
* test des sorties realais
* on envoie le code dans les datas afin de tester les sortie
* les datas vont de 0 à 10 pour les sortie relais
*
*
*
*
*/

boolean relay_test(int datas)
{

  int echantillons, sorties, Timout, compteur_sorties;
  float moyenne;
  float Table_Sorties[10] = {0};
  byte Pins[] = {A0, A1, A2, A3, A4, A5, A6, A7, A8, A9};
  compteur_sorties = 0;

  // attente que la sortie relay passe à 1 avec un timout si il y a un probleme et
  // retourne erreur  si les data sont  a 0 on attend rien
  if (datas != 0)
  {
    Timout = 0;

    while (((analogRead(Pins[datas - 1]) * 3.3) / 1024) < 2.7)
    {
      Timout++;
      delayMicroseconds(100);
      if (Timout >= 200)
      {
        json_send_terminal("Timout sortie");
        return false;
      }
    }
  }

  // stock dans un tableau les valeur des convertisseurs AD convertit aussi en tenssion.
  // pour éviter de mesurer un parasite on fait une moyenne sur 10 echantillons

  for (sorties = 0; sorties < 10; sorties++)
  {
    for (echantillons = 0; echantillons < 10; echantillons++)
    {
      moyenne = moyenne + ((analogRead(Pins[sorties]) * 3.3) / 1024);
    }
    moyenne = moyenne / 10;
    Table_Sorties[sorties] = moyenne;
  }

  // table des sortie test
  // test si il y a plusieurs fois une valeur dans le tableau

  for (sorties = 0; sorties < 10; sorties++)
  {

    if (Table_Sorties[sorties] > 3.0)
      compteur_sorties++;
    if (compteur_sorties > 1)
    {
      json_send_terminal("il y a un pont sur les sorties");
      return false;
    }
    if (compteur_sorties > 0 && datas == 0)
    {

      json_send_terminal("pas de sortie activee");
      return false;
    }
  }
  return true;
}

/*
*
*
*     test du bouton 
*     laisse à l'utilisateur 20 sec pour presser sur le bouton.
*
*/

boolean test_bouton(void)
{
  int timer_bouton = 0;
  json_send_terminal("tu as 20 secondes pour presser sur le bouton");
  delay(500);

  while (analogRead(A10) >= 300)
  {

    if (timer_bouton == 100)
      json_send_terminal("il reste 10 sec ... ");
    timer_bouton++;
    delay(100);
    if (timer_bouton >= 200)
      return false;
  }
  while (analogRead(A10) < 300)
    delay(2);
  return true;
}

/*
*
* mode master de la poir envoie l'adresse 27 avec 0 data
* 
* 
*
*/

void int_adr27(void)
{
  SendDatas_prise(27, 0);
  delayMicroseconds(600);
  SendDatas_prise(27, 0);
}

/**
 * @brief test de la poire, recoit le json et test selon les codes reçus 
 *        il retournera une erreur si il un code est faux ou un timout après 20 secondes
 * 
 * 
 * @param json_codes 
 * @return boolean 
 */

boolean test_poire(char *json_codes)

{

  JsonParser<500> parser;
  JsonHashTable codes_poire = parser.parseHashTable(json_codes);
  // test si le parsing a réussis
  if (!codes_poire.success())
  {
    Serial.println("{\"typemsg\":\"communication\",\"Error\"{\"type\":\"parsing_error_codes_poire\"}}");
    // abandon car erreur
    return false;
  }
  Serial.println("test_poire_json");
  JsonArray buttons = codes_poire.getArray("buttons");
  return true;

  // si la poire envoie un 0 c'est qu'aucune n'est pressée ou si le code est egal au dernier code il y a peut être une répétition
}

/**
 * @brief test la touche d'une poir attend et regarde si la valeur envoyé correspond à a la touche pressée
 * 
 * @param touche 
 * @return boolean 
 */
boolean test_touche(int touche)
{
  // timeout si aucune touches est détectée en 20 secondes
  int timeout_code, code_touche, code_old_touche;
  code_touche = 0;
  timeout_code = 0;
  code_old_touche = 0;
  while (code_touche == 0 || code_touche == code_old_touche)
  {
    int_adr27();
    //delay(240);
    code_touche = lecture_Bus_prise();
    delay(200);
    //Serial.println(lecture_Bus_prise());
  }
  return true;
}

/**
 * @brief test de la partie réseau avec le deuxième arduino 
 *        afin de tester cet partie nous attendront max 10 seconde que l'arduino réponde
 *        sinon le test sera considéré comme échoué
 *        le port uart3 sera aussi utilisé pour les tests de la poire c'est pour cela que nous l'initialison à chauqe fois
 * 
 * @return true 
 * @return false 
 */

boolean test_ip(void)
{
  char inChar;
  String strdatas = "";
  Serial3.begin(115200);
  int timeout = 0;
  // cette commande envoie un resetdes d'usine au configurateur de plus il test la partie réseau.
  Serial3.print("(RESET)");
  while (timeout <= 1000)
  {
    timeout += 1;
    while (Serial3.available())
    {

      // get the new byte:
      inChar = (char)Serial3.read();
      strdatas = strdatas + inChar;
    }
    if (strdatas.indexOf("<TCP-OK>") >= 0)
    {
      return true;
    }
    if (strdatas.indexOf("<TCP-KO>") >= 0)
    {
      return false;
    }
    delay(10);
  }
  return false;
}

/**
 * @brief 
 * 
 * @return true 
 * @return false 
 */
/*
bool TestESPA(void)
{

  Serial2.print("2\u0005");
  delay(3000);        // delay in between reads for stability
  int   TimeOutESPA = 0;
  boolean testESPA = false;
  while (TimeOutESPA < 2000 && testESPA == false)
  {
    while (Serial2.available()) {
      char inChar = Serial2.read();
      // si un EOT est detecté alors on valide le test
      if (inChar == 0x04)
      {
        return true;
        testESPA = true;
      }
      else
      {
        return false; 
      }
    }
    delay (1);
    TimeOutESPA++;
  }
  if (TimeOutESPA >= 2000)
  {
    return false;
  }
}

*/
/**
 * @brief envoie un reset à la platine du banc de tests
 * 
 * @return void 
 * @return void 
 */

void reset_24V(void)
{
  digitalWrite(RESET, LOW);
  delay(2000);
  digitalWrite(RESET, HIGH);
  // attente que le module reboot
  delay(1000);
}

/**
 * @brief test du convertisseur gets/gec
 * envoie au module l'ordre de se mettre en mode test 
 * attente de 40 sec max d'une réponse
 * @return void 
 * @return void 
 */

bool test_gec_gets(void)
{

  long timeout = 0;
  long can_id = 0;
  //0xB48141         0x398000
  SendDataCan1_aved_id(0x3980000, "00000000");
  timeout = millis();

  // test afin de voir si l'id reçus est bien celle du test un timeout de 30 seconde est aussi configuré
  while (((can_id & 0xFFF0000) != 0x3990000) && ((millis() - timeout) < 30000))
  {
    can_id = CanPrincipalReception_id() & 0xFFF0000;
  }
  // si l'id est = a 399 on return le test réussis sinon on retourn un echec
  if ((can_id & 0xFFF0000) == 0x3990000)
  {
    // on reset le module pour le remettre dans un mode normal
    reset_24V();
    return true;
  }
  else
  {
    return false;
  }
  return false;
}

/**
 * @brief envoie de la mac adresse
 * attente de 40 sec max d'une réponse
 * @return void 
 * @return void 
 */

bool prog_adresse_mac(char *macadresse)
{

  long timeout = 0;
  long can_id = 0;
  //0xB48141         0x398000

  SendDataCan1_aved_id(0x3860000, macadresse);
  timeout = millis();

  // test afin de voir si l'id reçus est bien celle du test un timeout de 30 seconde est aussi configuré
  while (((can_id & 0xFFF0000) != 0x3860000) && ((millis() - timeout) < 30000))
  {
    can_id = CanPrincipalReception_id() & 0xFFF0000;
  }
  // si l'id est = a 399 on return le test réussis sinon on retourn un echec
  if ((can_id & 0xFFF0000) == 0x3860000)
  {
    if (test_ip())
    {
      // on reset le module pour le remettre dans un mode normal
      reset_24V();
      return true;
    }
    else
    {
      test_result("Network (ip) ko", false);
    }

    return false;
  }
}

  /**
 * @brief test de l'appuie sur un bouton ou sur les deux 
 * exemple reception du numero de serie
 * @return void 
 * @return state 
 */

  bool test_boutton(void)
  {

    long timeout = 0;
    long can_id = 0;
    //0xB48141         0x398000

    //SendDataCan1_aved_id(0x3860000, "1360000");
    timeout = millis();

    json_send_terminal("Tu as 20 secondes pour presser sur le/les bouton/s");
    // test afin de voir si l'id reçus est bien celle du test un timeout de 20 seconde est aussi configuré
    while (((can_id & 0xFFF0000) != 0x1360000) && ((millis() - timeout) < 20000))
    {
      can_id = CanPrincipalReception_id() & 0xFFF0000;
    }
    // si l'id est = a 399 on return le test réussis sinon on retourn un echec
    if ((can_id & 0xFFF0000) == 0x1360000)
    {
      return true;
    }

    return false;
  }

  /**
 * @brief reset le programmateur stlink
 * exemple reception du numero de serie
 * @return void 
 * @return void 
 */
  void reset_stlink(void)
  {
    // de la tension revient par le connecteur de prog alors on reset tout
    digitalWrite(RESET_STLINK, LOW);
    digitalWrite(RESET, LOW);
    delay(1000);
    digitalWrite(RESET_STLINK, HIGH);
    digitalWrite(RESET, HIGH);
    // attente que le module reboot
  }

  /**
 * @brief envoie du numero de serie au produit
 * attente de 40 sec max d'une réponse
 * @return void 
 * @return void 
 */

  bool prog_serie(char *serial)
  {

    long timeout = 0;
    long can_id = 0;
    //0xB48141         0x398000

    SendDataCan1_aved_id(0x3840000, serial);
    timeout = millis();

    // test afin de voir si l'id reçus est bien celle du test un timeout de 30 seconde est aussi configuré
    while (((can_id & 0xFFF0000) != 0x3840000) && ((millis() - timeout) < 30000))
    {
      can_id = CanPrincipalReception_id() & 0xFFF0000;
    }

    /*


    ATTENTION ICI IL FAUDRA TESTER !!


  */
    // si l'id est = a 399 on return le test réussis sinon on retourn un echec
    if ((can_id & 0xFFF0000) == 0x3840000)
    {
      // on reset le module pour le remettre dans un mode normal
      reset_24V();
      return true;
    }
    else
    {
      json_send_error("GUI", "erreur lors de la programmation du numero de série");
      return false;
    }
    return false;
  }