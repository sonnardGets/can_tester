/*
 * Filename: c:\Users\Sonnardni\Documents\Arduino\lecture_bus_gets_poires\bus_poire.h
 * Path: c:\Users\Sonnardni\Documents\Arduino\lecture_bus_gets_poires
 * Created Date: Tuesday, February 11th 2020, 5:10:17 pm
 * Author: Nicolas.Sonnard
 * 
 * Copyright (c) 2020 Your Company
 */

/****************************************************************************
  Description : constante pour les timming du bus
*****************************************************************************/
#define COURTE          38
#define LONGUE          256
#define LONGUE_PRISE    112
#define COURTE_PRISE    28
#define INTERVAL        1220

#include"delay_ns.h"
#include <Arduino.h>



// prototypes des diverses fonctions

uint8_t Conversion_trinaire_vers_decimal(char *bits_trinaires);
int lecture_Bus(void);
void SendDatas(int TxAdresse,int datas);
void SendABit(int trinaire);
void ini_io_bus(void);
void SendDatas_prise(int TxAdresse, int datas);
void SendABit_prise(int trinaire);
int lecture_Bus_prise();
