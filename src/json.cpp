/*
 * Filename: c:\Users\Sonnardni\Documents\Arduino\lecture_bus_gets_poires\Json.c
 * Path: c:\Users\Sonnardni\Documents\Arduino\lecture_bus_gets_poires
 * Created Date: Tuesday, February 11th 2020, 2:52:22 pm
 * Author: Nicolas.Sonnard
 * 
 * Copyright (c) 2020 Your Company
 */

#include "json.h"
#include <Arduino.h>

/*

    envoie d'une erreur sous form JSON

    ("{\"typemsg\":\"communication\",\"Error\":{\"type\":\"ERROR\"}}");

    reception -   erreur
    return        AUCUN

*/

void json_send_error(char *typemsg, char *error)
{

  Serial.print("{\"typemsg\":\"");
  Serial.print(typemsg);
  Serial.print("\",\"Error\":{\"type\":\"");
  Serial.print(error);
  Serial.println("\"}}");
}

/*

    envoie d'un message au GUI

    ("{\"typemsg\":\"GUI\",\"Error\":{\"type\":\"ERROR\"}}");

    reception -   erreur
    return        AUCUN

  Question          	For asking a question during normal operations.

	Information	            For reporting information about normal operations.

	Warning	                For reporting non-critical errors.

	Critical	              For reporting critical errors.

*/

void json_send_msgbox_gui(char *txt_msg, int msgbox_type)
{
  char *msg_box[] = {"Question", "Information", "Warning", "Critical"};

  Serial.print("{\"typemsg\":\"GUI\",\"msgbox\":{\"type\":\"");
  Serial.print(msg_box[msgbox_type]);
  Serial.print(",\"text\":\"");
  Serial.print(txt_msg);
  Serial.println("\"}}");
}

/*

    envoie d'un message au terminal du gui sous form JSON

    ("{\"typemsg\":\"GUI\",\"Error\":{\"type\":\"ERROR\"}}");

    reception -   erreur
    return        AUCUN

*/

void json_send_terminal(char *txt)
{

  Serial.print("{\"typemsg\":\"GUI\",\"terminal\":{\"txt\":\"");
  Serial.print(txt);
  Serial.println("\"}}");
}

/*

    envoie d'une valeur au terminal du gui sous form JSON

    ("{\"typemsg\":\"GUI\",\"Error\":{\"type\":\"ERROR\"}}");

    reception -   value
    return        AUCUN

*/

void json_send_value_terminal(int value)
{

  Serial.print("{\"typemsg\":\"GUI\",\"terminal\":{\"txt\":\"");
  Serial.print(value);
  Serial.println("\"}}");
}

void test_result(char *txt, boolean test_result)
{
  // si les tests sont réussis on fait ça
  if (test_result)
  {
    if (strlen(txt) > 1)
    {
      json_send_terminal(txt);
    }

  }
  // sinon
  else
  {
    if (strlen(txt) > 1)
    {
      json_send_terminal(txt);
    }
    json_send_terminal("un test n a pas reussis");
    Serial.print("{\"typemsg\":\"tests\",\"result\":{\"global_tests\":\"");
    Serial.print("false");
    Serial.println("\"}}");
  }
  return;
}



/*

    envoie du courant

    ("{\"typemsg\":\"GUI\",\"Error\":{\"type\":\"ERROR\"}}");

    reception -   erreur
    return        AUCUN

*/

void json_send_current( float current)
{
  Serial.print("{\"typemsg\":\"");
  Serial.print("GUI");
  Serial.print("\",\"courant\":\"");
  Serial.print(current);
  Serial.println("\"}");
}


/*

    envoie de la version de soft

    ("{\"typemsg\":\"GUI\",\"Error\":{\"type\":\"ERROR\"}}");

    reception -   erreur
    return        AUCUN

*/

void json_send_soft( float soft)
{
  Serial.print("{\"typemsg\":\"");
  Serial.print("GUI");
  Serial.print("\",\"soft\":\"");
  Serial.print(soft);
  Serial.println("\"}");
}


/*

    envoie un json sur la première page du GUI sous forme d'infos ex 230V débranché

    ("{\"typemsg\":\"GUI\",\"infos_box\":\"230V KO\"}");

    reception -   erreur
    return        AUCUN

*/
void json_send_infos_box(char *txt)

{
  Serial.print("{\"typemsg\":\"");
  Serial.print("GUI");
  Serial.print("\",\"infos_box\":\"");
  Serial.print(txt);
  Serial.println("\"}");
}

/*

    clear terminal

    ("{\"typemsg\":\"GUI\",\"Error\":{\"type\":\"ERROR\"}}");

    reception -   erreur
    return        AUCUN

*/

void clear_main_terminal(void)

{
  Serial.print("{\"typemsg\":\"");
  Serial.print("GUI");
  Serial.print("\",\"terminal\":\"");
  Serial.print("clear");
  Serial.println("\"}");
}