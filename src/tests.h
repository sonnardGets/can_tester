/*
 * Filename: c:\Users\Sonnardni\Documents\Arduino\lecture_bus_gets_poires\tests.h
 * Path: c:\Users\Sonnardni\Documents\Arduino\lecture_bus_gets_poires
 * Created Date: Tuesday, February 11th 2020, 5:07:04 pm
 * Author: Nicolas.Sonnard
 * 
 * Copyright (c) 2020 Your Company
 */
/****************************************************************************
  entrees/sorties
*****************************************************************************/


#include"bus_poire.h"
#include"delay_ns.h"
#include "json.h"
#include <JsonParser.h>
#include "can_library_ns.h"
#include "defines.h"






boolean tests_p2p4p5(int relay_1, int nb_relay);
boolean appels_electronique();
boolean relay_test(int datas);
boolean mesure_pinx(int pin);
void json_traitement(char* InputBufer);
boolean test_bouton(void);
boolean test_poire(char* json_codes);
boolean test_touche(int touche);
void int_adr27(void);
boolean test_ip(void);
void reset_24V(void);
bool test_gec_gets(void);
bool test_boutton(void);
void reset_stlink(void);
bool prog_adresse_mac(char *macadresse);
bool prog_serie(char *serial);