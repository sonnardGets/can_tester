/*
 * Filename: c:\Users\Sonnardni\Documents\Arduino\lecture_bus_gets_poires\bus_poire.c
 * Path: c:\Users\Sonnardni\Documents\Arduino\lecture_bus_gets_poires
 * Created Date: Tuesday, February 11th 2020, 5:10:05 pm
 * Author: Nicolas.Sonnard
 * 
 * Copyright (c) 2020 Your Company
 */

#include "bus_poire.h"
#include <Arduino.h>

/*

    init des pin pour la lecture du bus 

    param --> aucun

*/

void ini_io_bus(void)
{

  PMC->PMC_PCER0 = PMC_PCER0_PID13; // PIOC power on

  //Enable PB27 and PC12 (Peripheral Enable Register)
  PIOC->PIO_PER = PIO_PC13;
  PIOC->PIO_PER = PIO_PC12;

  //Set B27 as output (Output Enable Register)
  PIOC->PIO_OER = PIO_PC13;

  //Set C12 as input (Ouput Disable Register)
  PIOC->PIO_ODR = PIO_PC12;

  //Disable pull-up on both pins (Pull Up Disable Register)
  PIOC->PIO_PUDR = PIO_PC13;
  PIOC->PIO_PUDR = PIO_PC12;

  PIOC->PIO_CODR |= PIO_PC13; //Clear Output Data Register
}

/****************************************************************************/
/**
  \brief   D�codage de l'adresse trinaire: traduit l'adresse trinaire en un entier.
  \param  bits_trinaires�[] : tableau d'entiers repr�sentant les bits trinaires
  \return   uint8_t : adresse d�cimale d�cod�e.
  \bug      Pas de bug pour l'instant, mais il n'y a pas de v�rification pour garantir qu'il n'y audra
            pas un appel en dehors des dimensions du tableau. A faire � l'occasion...
*****************************************************************************/
uint8_t Conversion_trinaire_vers_decimal(char *bits_trinaires)
{
  uint8_t z;
  uint8_t adresse;

  adresse = 0;

  /* Construction de l'adresse d�cimale */
  for (z = 0; z < 5; z++)
  {

    adresse = adresse + (bits_trinaires[z] * pow(3, z));
  }

  return adresse;
}

/*
*   lecture du bus 
*
*   entrée aucun
*   renvoie l'aresse
*
*/

int lecture_Bus()
{

  long etatold;
  int data_deciaml;

  int TimmingOld, Timming, k, A, MultipleTimer;
  MultipleTimer = 0;
  Timming = 0;
  k = 0;

  etatold = (PIOC->PIO_PDSR & PIO_PC12);

  long timmingtable[30] = {0};
  int statetable[50] = {0};
  char trinairetable[50] = {0};

  while (k < 17)
  {
    if ((PIOC->PIO_PDSR & PIO_PC12) == 0)
    {
      Timming = 0;

      while ((PIOC->PIO_PDSR & PIO_PC12) == 0)
      {
        //delayMicroseconds(1);
        Timming++;
        Delay1us();
      }
      //MultipleTimer=GetFrequency(Timming);

      if (Timming >= 60 && Timming <= 100)
      {
        timmingtable[k] = Timming;
        // on inverse l'etat car denis l'inverse ...
        statetable[k] = 1;
        k++;
      }

      // on reset si un data est long c'est entre deux trames
      if (Timming >= 200)
      {
        k = 0;
      }
    }
    /*
        tests a l'état haut
    */

    if ((PIOC->PIO_PDSR & PIO_PC12) == PIO_PC12)
    {
      Timming = 0;

      while ((PIOC->PIO_PDSR & PIO_PC12) == PIO_PC12)
      {
        //delayMicroseconds(1);
        Timming++;
        Delay1us();
      }
      //MultipleTimer=GetFrequency(Timming);

      if (Timming >= 60 && Timming <= 100)
      {
        timmingtable[k] = Timming;
        // on inverse l'etat car denis l'inverse ...
        statetable[k] = 0;
        k++;
      }

      // on reset si un data est long c'est entre deux trames
      if (Timming >= 200)
      {
        k = 0;
      }
    }
  }
  // set le dernier bit selon l'avant dernier car il y a pas d'interbit au dernier bit et ils sont en couple de 2.
  if (k == 17 && statetable[16] == 0)
    statetable[17] = 0;
  if (k == 17 && statetable[16] == 1)
    statetable[17] = 1;

  if (k >= 17)

  {

    A = 0;
    for (k = 0; k < 20; k += 2)
    {

      int addition = statetable[k] + statetable[k + 1];
      //Serial.println(addition);
      switch (addition)
      {
      case 0:
        trinairetable[A] = 0;
        break;
      case 1:
        trinairetable[A] = 2;
        break;
      case 2:
        trinairetable[A] = 1;
        break;
      default:
        Serial.print("Error");
        break;
      }
      A++;
    }
    // convertit le data en décimal
    data_deciaml = (statetable[10] + statetable[11]) * 1;
    data_deciaml = data_deciaml + (((statetable[12] + statetable[13]) / 2) * 2);
    data_deciaml = data_deciaml + (((statetable[14] + statetable[15]) / 2) * 4);
    data_deciaml = data_deciaml + (((statetable[16] + statetable[17]) / 2) * 8);

    /*Serial.print(statetable[10]);
    Serial.print(statetable[11]);
    Serial.print(" -- ");
    Serial.print(statetable[12]);
    Serial.print(statetable[13]);
    Serial.print(" -- ");
    Serial.print(statetable[14]);
    Serial.print(statetable[15]);
    Serial.print(" -- ");
    Serial.print(statetable[16]);
    Serial.println(statetable[17]);
    */

    /*
    Serial.print("adresse -- >   ");
    Serial.println(Conversion_trinaire_vers_decimal(trinairetable));

    
      Serial.println("Table\t\t timming\t\t brut\t\t trinaire");

      for (k = 0; k < 20; k++)
      {

      Serial.print(k);
      Serial.print("\t\t");
      Serial.print(timmingtable[k]);
      Serial.print("\t\t");
      Serial.print(statetable [k]);
      Serial.print("\t\t");
      Serial.print(int (trinairetable[k]));

      Serial.println("");
      }
    */
    TimmingOld = Timming;
    //SASREP();
    //k = 0;
  }

  return data_deciaml;
}

/*
*   envoie data
*
*   entrée adresse - data
*   renvoie rien
*
*/

void SendDatas(int TxAdresse, int datas)
{

  int p;
  int bit;

  // envoie de l'adresse
  for (p = 0; p < 5; p++)
  {
    //rep[i]=adresse%3;

    bit = TxAdresse % 3;

    SendABit(bit);
    TxAdresse = floor(TxAdresse / 3);
  }

  // envoie de chaque bit de data via un module ces bits sont en binaires
  /*
      Serial.println("");
      Serial.print(datas%2);
      Serial.print((datas/2)%2);
      Serial.print((datas/4)%2);
      Serial.print((datas/6)%2);
      Serial.println("-----");
      */

  SendABit(datas % 2);
  SendABit((datas / 2) % 2);
  SendABit((datas / 4) % 2);
  SendABit((datas / 8) % 2);
}

/*
*
*  envoie des data sur le bus bit par bit
*   
*  0 --> 0
*  1 --> 1
*  2 --> X
*/

void SendABit(int trinaire)
{

  // selon la valeur du bit on envoie sur le bus
  switch (trinaire)
  {
  case 0:
  {

    // genere le bit 0
    PIOC->PIO_SODR |= PIO_PC13; //Clear Output Data Register
    Delayus(COURTE);            // wait for a second
    PIOC->PIO_CODR |= PIO_PC13; //Set Ouput Data Register // turn the LED on (HIGH is the voltage level)
    Delayus(LONGUE);
    PIOC->PIO_SODR |= PIO_PC13; //Clear Output Data Register
    Delayus(COURTE);            // wait for a second
    PIOC->PIO_CODR |= PIO_PC13; //Set Ouput Data Register // turn the LED on (HIGH is the voltage level)
    Delayus(LONGUE);
  }
  break;
  case 1:
  {

    // genere le bit 1
    PIOC->PIO_SODR |= PIO_PC13; //Clear Output Data Register
    Delayus(LONGUE);            // wait for a second
    PIOC->PIO_CODR |= PIO_PC13; //Set Ouput Data Register // turn the LED on (HIGH is the voltage level)
    Delayus(COURTE);
    PIOC->PIO_SODR |= PIO_PC13; //Clear Output Data Register
    Delayus(LONGUE);            // wait for a second
    PIOC->PIO_CODR |= PIO_PC13; //Set Ouput Data Register // turn the LED on (HIGH is the voltage level)
    Delayus(COURTE);
  }
  break;
  case 2:
  {

    // genere le bit X
    PIOC->PIO_SODR |= PIO_PC13; //Clear Output Data Register
    Delayus(LONGUE);            // wait for a second
    PIOC->PIO_CODR |= PIO_PC13; //Set Ouput Data Register // turn the LED on (HIGH is the voltage level)
    Delayus(COURTE);
    PIOC->PIO_SODR |= PIO_PC13; //Clear Output Data Register
    Delayus(COURTE);            // wait for a second
    PIOC->PIO_CODR |= PIO_PC13; //Set Ouput Data Register // turn the LED on (HIGH is the voltage level)
    Delayus(LONGUE);
  }
  break;
  }
}

/*

     annalyse de la fréquence de l'interbit
     On part du principe que nous travaillons avec des fréquences rapides
     fréquence rapide on renvoie 1
     pour la fréquence moyenne on renvoie 2
     fréquence lente 4
     ces valeur retournée seront utilisée pour multiplier la constante de temps
     
     0
     1
     2

     fréq   rapide    >3    &&    <7
     fréq   moyenne   >8    &&    <15
     fréq   lente     >15   &&    <27



*/

int GetFrequency(int timmingFreq)
{
  //Serial.println(timmingFreq);
  // on test si c'est un interbit
  if (timmingFreq > 3 && timmingFreq < 27)
  {
    // test si c'est une fréquence rapide
    if (timmingFreq > 3 && timmingFreq < 7)
      return 1;
    // test si c'est une fréquence moyenne
    if (timmingFreq > 8 && timmingFreq < 15)
      return 2;
    // test si c'est une fréquence lente
    if (timmingFreq > 15 && timmingFreq < 27)
      return 4;
  }
}

void SendDatas_prise(int TxAdresse, int datas)
{

  int p;
  int bit;

  // envoie de l'adresse
  for (p = 0; p < 5; p++)
  {
    //rep[i]=adresse%3;

    bit = TxAdresse % 3;

    SendABit_prise(bit);
    TxAdresse = floor(TxAdresse / 3);
  }

  // envoie de chaque bit de data via un module ces bits sont en binaires
  /*
      Serial.println("");
      Serial.print(datas%2);
      Serial.print((datas/2)%2);
      Serial.print((datas/4)%2);
      Serial.print((datas/6)%2);
      Serial.println("-----");
      */

  SendABit_prise(datas % 2);
  SendABit_prise((datas / 2) % 2);
  SendABit_prise((datas / 4) % 2);
  SendABit_prise((datas / 8) % 2);
}

/*
*
*  envoie des data sur le bus bit par bit
*   
*  0 --> 0
*  1 --> 1
*  2 --> X
*/

void SendABit_prise(int trinaire)
{

  // selon la valeur du bit on envoie sur le bus
  switch (trinaire)
  {
  case 0:
  {

    // genere le bit 0
    PIOC->PIO_SODR |= PIO_PC13; //Clear Output Data Register
    Delayus(COURTE_PRISE);      // wait for a second
    PIOC->PIO_CODR |= PIO_PC13; //Set Ouput Data Register // turn the LED on (HIGH is the voltage level)
    Delayus(LONGUE_PRISE);
    PIOC->PIO_SODR |= PIO_PC13; //Clear Output Data Register
    Delayus(COURTE_PRISE);      // wait for a second
    PIOC->PIO_CODR |= PIO_PC13; //Set Ouput Data Register // turn the LED on (HIGH is the voltage level)
    Delayus(LONGUE_PRISE);
  }
  break;
  case 1:
  {

    // genere le bit 1
    PIOC->PIO_SODR |= PIO_PC13; //Clear Output Data Register
    Delayus(LONGUE_PRISE);      // wait for a second
    PIOC->PIO_CODR |= PIO_PC13; //Set Ouput Data Register // turn the LED on (HIGH is the voltage level)
    Delayus(COURTE_PRISE);
    PIOC->PIO_SODR |= PIO_PC13; //Clear Output Data Register
    Delayus(LONGUE_PRISE);      // wait for a second
    PIOC->PIO_CODR |= PIO_PC13; //Set Ouput Data Register // turn the LED on (HIGH is the voltage level)
    Delayus(COURTE_PRISE);
  }
  break;
  case 2:
  {

    // genere le bit X
    PIOC->PIO_SODR |= PIO_PC13; //Clear Output Data Register
    Delayus(LONGUE_PRISE);      // wait for a second
    PIOC->PIO_CODR |= PIO_PC13; //Set Ouput Data Register // turn the LED on (HIGH is the voltage level)
    Delayus(COURTE_PRISE);
    PIOC->PIO_SODR |= PIO_PC13; //Clear Output Data Register
    Delayus(COURTE_PRISE);      // wait for a second
    PIOC->PIO_CODR |= PIO_PC13; //Set Ouput Data Register // turn the LED on (HIGH is the voltage level)
    Delayus(LONGUE_PRISE);
  }
  break;
  }
}

int lecture_Bus_prise()
{

  long etatold;
  int data_deciaml;

  int TimmingOld, k, A, MultipleTimer;
  MultipleTimer = 0;
  long Timming = 0;
  k = 0;
  long timout_error = 0;

  etatold = (PIOC->PIO_PDSR & PIO_PC12);

  long timmingtable[30] = {0};
  int statetable[50] = {0};
  char trinairetable[50] = {0};
  // synchro du bus
  while ((PIOC->PIO_PDSR & PIO_PC12) == PIO_PC12)
  {
    if (timout_error >= 1000000)
    {
      Serial.println("timout bus_1");
      return 0xff;
    }
    else
    {
      timout_error++;
      Delay1us();
    }
  }
  timout_error = 0;
  while (k < 17)
  {
    // timout si pas de data pendant 300 ms
    if (timout_error >= 300000)
    {
      Serial.println("timout bus");
      return 0xff;
    }
    timout_error++;
    Delay1us();
    if ((PIOC->PIO_PDSR & PIO_PC12) == 0)
    {
      Timming = 0;

      while ((PIOC->PIO_PDSR & PIO_PC12) == 0)
      {
        //delayMicroseconds(1);
        Timming++;
        Delay1us();
      }
      //MultipleTimer=GetFrequency(Timming);

      if (Timming >= 100 && Timming <= 210)
      {
        timmingtable[k] = Timming;
        // on inverse l'etat car denis l'inverse ...
        statetable[k] = 1;
        k++;
      }

      // on reset si un data est long c'est entre deux trames
      if (Timming >= 300)
      {
        k = 0;
      }
    }
    /*
        tests a l'état haut
    */

    if ((PIOC->PIO_PDSR & PIO_PC12) == PIO_PC12)
    {
      Timming = 0;

      while ((PIOC->PIO_PDSR & PIO_PC12) == PIO_PC12)
      {
        if (Timming >= 300000)
        {
          Serial.println("timout bus_2");
          return 0xff;
        }
        //delayMicroseconds(1);
        Timming++;
        Delay1us();
      }
      //MultipleTimer=GetFrequency(Timming);

      if (Timming >= 100 && Timming <= 210)
      {
        timmingtable[k] = Timming;
        // on inverse l'etat car denis l'inverse ...
        statetable[k] = 0;
        k++;
      }

      // on reset si un data est long c'est entre deux trames
      if (Timming >= 300)
      {
        k = 0;
      }
    }
  }
  // set le dernier bit selon l'avant dernier car il y a pas d'interbit au dernier bit et ils sont en couple de 2.
  if (k == 17 && statetable[16] == 0)
    statetable[17] = 0;
  if (k == 17 && statetable[16] == 1)
    statetable[17] = 1;

  if (k >= 17)

  {

    A = 0;
    for (k = 0; k < 20; k += 2)
    {

      int addition = statetable[k] + statetable[k + 1];
      //Serial.println(addition);
      switch (addition)
      {
      case 0:
        trinairetable[A] = 0;
        break;
      case 1:
        trinairetable[A] = 2;
        break;
      case 2:
        trinairetable[A] = 1;
        break;
      default:
        Serial.print("Error");
        break;
      }
      A++;
    }
    // convertit le data en décimal
    data_deciaml = ((statetable[10] + statetable[11])/2) * 1;
    data_deciaml = data_deciaml + (((statetable[12] + statetable[13]) / 2) * 2);
    data_deciaml = data_deciaml + (((statetable[14] + statetable[15]) / 2) * 4);
    data_deciaml = data_deciaml + (((statetable[16] + statetable[17]) / 2) * 8);

    /*Serial.print(statetable[10]);
    Serial.print(statetable[11]);
    Serial.print(" -- ");
    Serial.print(statetable[12]);
    Serial.print(statetable[13]);
    Serial.print(" -- ");
    Serial.print(statetable[14]);
    Serial.print(statetable[15]);
    Serial.print(" -- ");
    Serial.print(statetable[16]);
    Serial.println(statetable[17]);
    */
    /*
    Serial.println("######################################   ");
    Serial.print("adresse -- >   ");
    Serial.print("\t\t\t data --> \t\t");
    Serial.println(data_deciaml);
    Serial.println(Conversion_trinaire_vers_decimal(trinairetable));

    Serial.println("Table\t\t timming\t\t brut\t\t trinaire");

    for (k = 0; k < 20; k++)
    {

      Serial.print(k);
      Serial.print("\t\t");
      Serial.print(timmingtable[k]);
      Serial.print("\t\t");
      Serial.print(statetable[k]);
      Serial.print("\t\t");
      Serial.print(int(trinairetable[k]));

      Serial.println("");
    }

    Serial.println("donne brut \t\t\t");
    Serial.print (statetable[10]);
    Serial.print ("\t");
    Serial.print (statetable[11]);
    Serial.print ("\t");
    Serial.print (statetable[12]);
    Serial.print ("\t");
    Serial.print (statetable[13]);
    Serial.print ("\t");
    Serial.print (statetable[14]);
    Serial.print ("\t");
    Serial.print (statetable[15]);
    Serial.print ("\t");
    Serial.print (statetable[16]);
    Serial.print ("\t");
    Serial.println (statetable[17]);
    


    Serial.println("-----------------------------------------");

    TimmingOld = Timming;
    */
    //SASREP();
    //k = 0;
  }

  return data_deciaml;
}