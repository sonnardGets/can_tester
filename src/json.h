/*
 * Filename: c:\Users\Sonnardni\Documents\Arduino\lecture_bus_gets_poires\json.h
 * Path: c:\Users\Sonnardni\Documents\Arduino\lecture_bus_gets_poires
 * Created Date: Tuesday, February 11th 2020, 3:04:36 pm
 * Author: Nicolas.Sonnard
 * 
 * Copyright (c) 2020 Your Company
 */


#include <Arduino.h>
//#include"tests.h"
#include <string.h>





void json_send_error(char* typemsg, char* error);
void json_send_msgbox_gui(char* txt_msg,int msgbox_type);
void json_send_terminal(char *txt);
void json_send_value_terminal(int value);
void test_result(char*txt,boolean test_result);
void json_send_current( float current);
void json_send_soft( float soft);
void json_send_infos_box(char *txt);
void clear_main_terminal(void);
