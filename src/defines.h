/*
*
*
*   Contient les define des pins utilisee
*
*/


#define T2cmd               7
#define T2Mesure            4
#define T3cmd               6
#define T3Mesure            5
#define ACCcmd              11
#define ACCcmes             8
#define Appele_jack         12
#define A1cmd               2
#define A1cmes              3
#define RESET               23
#define RESET_STLINK        22